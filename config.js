let screenHeight = window.screen.height;
let screenWidth = window.screen.width;

export const config = {
    gameBoardHeight: screenHeight ? screenHeight : 400,
    gameBoardWidth: screenWidth ? screenWidth : 600,
    gameBoardBackgroundColor: "#000",
};

export const pacmanConfig = {
    pacmouth: 320,
    pacdir: 0,
    sWidth: 32,
    sHeight: 32,
    x: 50,
    y: 100,
    psizeX: 32,
    psizeY: 32,
    speed: 5,
}
