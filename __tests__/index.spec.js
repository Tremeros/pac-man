// import { describe } from "yargs";
import * as utils from '../utils.js';


describe('First test', () => {
    it('is a first test', () => {
    // given
        const number = 3;

    // When

    const result = utils.testFunction(number);

    // then

    expect(result).toStrictEqual(5);
    })
})