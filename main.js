import {
    config,
    pacmanConfig
 } from './config.js';

 import {
     mapObject,
     DrawImage,
     createCanvas,
 } from './utils.js';

 let pacmanScore = 0;
 let ghostScore = 0;


//  const music = new Audio('pacman_chomp.wav');
//  let body = document.querySelector("body");
//  body.addEventListener("click", () => {
//     music.play();
//  })

// https://www.classicgaming.cc/classics/pac-man/sounds

const loadConfig = () => {
    const loadedConfig = mapObject(config);
    return loadedConfig;
}

const loadPacmanConfig = () => {
    const loadedConfig = mapObject(pacmanConfig);
    return loadedConfig;
}

const loadedConfig = loadConfig();
const loadedPacmanConfig = loadPacmanConfig();


export const createGameBoard = () => {
    const canvas = document.createElement("canvas");
    let context = canvas.getContext("2d");
    canvas.height = config.gameBoardHeight;
    canvas.width = config.gameBoardWidth;
    document.body.appendChild(canvas);

    return { context, canvas };
}

const { context, canvas } = createCanvas(loadedConfig.gameBoardWidth, loadedConfig.gameBoardHeight);

const mainImage = new Image();
mainImage.ready = false;
mainImage.onload = checkReady;
mainImage.src = "pac.png";

function checkReady() {
    this.ready = true;
    playGame(context, canvas, mainImage);
}

const playGame = (context, canvas, mainImage) => {
    render(context, canvas, mainImage);
}

const render = (context, canvas, mainImage) => {
    context.fillStyle = config.gameBoardBackgroundColor;
    context.fillRect(0, 0, canvas.width, canvas.height);
    DrawImage(context, mainImage, pacmanConfig);         // Render pacman
    context.font = "20px Verdana";
    context.fillStyle = "#fff";
    context.fillText(`Pacman: ${pacmanScore} vs Ghost ${ghostScore}`, 15, 30);
}

// Event Listeners

const keyClick = {};

document.addEventListener("keydown", (event) => {
   keyClick[event.key] = true;
   move(keyClick);
   console.log(keyClick)
}, false);

document.addEventListener("keyup", (event) => {
   delete keyClick[event.key]
}, false);


const move = (keyclick) => {

    loadedPacmanConfig.x ++;
   render(context, canvas, mainImage);
}

