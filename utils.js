

export const mapObject = (config = {}) => {
    if (typeof config !== 'object') return;

    const object = {};
    for (const [key, value] of Object.entries(config)) {
       object[key] = value;
    }
    return object;
}


export const DrawImage = (context, img, config) => {
    const {pacmouth, pacdir, sWidth, sHeight, x, y, psizeX, psizeY} = config;
    return context.drawImage(img, pacmouth, pacdir, sWidth, sHeight, x, y, psizeX, psizeY);
}

export const createCanvas = (width, height) => {
    const canvas = document.createElement("canvas");
    let context = canvas.getContext("2d");
    canvas.height = width;
    canvas.width = height;

    return { canvas, context };
}

export const createPacman = (context, canvas) => {
    const mainImage = new Image();
    mainImage.ready = false;
    mainImage.onload = checkReady;
    mainImage.src = "pac.png";

function checkReady() {
    this.ready = true;
    playGame(context, canvas, mainImage);
}
}

export const testFunction = (number) => {
     return number + 2;
}

